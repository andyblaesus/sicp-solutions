(define (enumerate-interval low high)
  (if (> low high)
      '()
      (cons low (enumerate-interval (+ low 1) high))))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

(define (all-true? pred seq)
  (accumulate (lambda (a b) (and a b))
              #t
              (map pred seq)))

; Main stuff

(define empty-board '())

(define (adjoin-position new-row k rest-of-queens)
  (append rest-of-queens (list (list new-row k))))

(define (safe? k positions)
  
  (define (last-element seq)
    (if (null? (cdr seq))
      (car seq)
      (last-element (cdr seq))))

  (define (no-attack? q1 q2)

    (define (same-row? q1 q2)
      (= (car q1) (car q2)))

    (define (same-diag? q1 q2)
      (= (abs (- (car q1) (car q2)))
         (abs (- (cadr q1) (cadr q2)))))

    (if (= (cadr q1) (cadr q2)) ; samelast queen
      #t
      (and (not (same-row? q1 q2))
           (not (same-diag? q1 q2)))))

  (let ((last-queen (last-element positions)))
    (all-true? (lambda (old-queen) (no-attack? old-queen last-queen))
               positions)))

(define (queens board-size)
  (define (queen-cols k)  
    (if (= k 0)
        (list empty-board)
        (filter
         (lambda (positions) (safe? k positions))
         (flatmap
          (lambda (rest-of-queens)
            (map (lambda (new-row)
                   (adjoin-position new-row k rest-of-queens))
                 (enumerate-interval 1 board-size)))
          (queen-cols (- k 1))))))
  (queen-cols board-size))

(queens 8)
