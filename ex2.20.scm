(define (same-parity a . l)
  (define pick?
    (if (even? a) even? odd?))
  (define (select l)
    (cond ((null? l) ())
          ((pick? (car l)) (cons (car l) (select (cdr l))))
          (else (select (cdr l)))))
  (cons a (select l)))

(same-parity 1 2 3 4 5 6 7)
(same-parity 2 3 4 5 6 7)
