;; version1
(define (origin-frame frame) (car frame))
(define (edge1-frame frame) (cdar frame))
(define (edge2-frame frame) (cddar frame))

;;version2
(define (edge2-frame frame) (cddr frame))
