(define (element-of-set? x set)  ; Same as before
  (cond ((null? set) #f)
        ((equal? x (car set)) #t)
        (else (element-of-set? x (cdr set)))))

(define (adjoin-set x set) ; Different!
  (cons x set))

(define (intersection-set set1 set2)  ; Same
  (cond ((or (null? set1) (null? set2)) '())
        ((element-of-set? (car set1) set2)        
         (cons (car set1)
               (intersection-set (cdr set1) set2)))
        (else (intersection-set (cdr set1) set2))))

(define (union-set set1 set2)  ; Different!
  (append set1 set2))

;; When the situation requires a lot of adjoin and unoin operations, we should use the new representation.
