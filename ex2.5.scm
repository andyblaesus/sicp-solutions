(define (pow x n)
  (if (= n 0)
    1
    (* x (pow x (- n 1)))))

(define (cons a b)
  (* (pow 2 a)
     (pow 3 b)))

(define (car x)
  (if (= (remainder x 2) 0)
    (+ (car (/ x 2)) 1)
    0))

(define (cdr x)
  (if (= (remainder x 3) 0)
    (+ (cdr (/ x 3)) 1)
    0))

;test
(define p (cons 24 6))
(car p)
(cdr p)
