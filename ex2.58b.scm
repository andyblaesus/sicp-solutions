(define (segment-before l n)
  (define (iter seg i)
    (if (= i n)
        '()
        (cons (car seg)
              (iter (cdr seg) (+ i 1)))))
  (iter l 1))


(define (segment-after l n)
  (define (iter seg i)
    (if (< i (+ n 1))
        (iter (cdr seg) (+ i 1))
        seg))
  (iter l 1))

(define (variable? x)  (symbol? x))

(define (same-variable? v1 v2)
  (and (variable? v1)  (variable? v2)  (eq? v1 v2)))

(define (=number? exp num)
  (and (number? exp)  (= exp num)))

(define (make-sum a1 a2)
  (cond ((=number? a1 0) a2)
        ((=number? a2 0) a1)
        ((and (number? a1)  (number? a2))  (+ a1 a2))
        (else (list '+ a1 a2))))

(define (make-product m1 m2)
  (cond ((or (=number? m1 0)  (=number? m2 0)) 0)
        ((=number? m1 1) m2)
        ((=number? m2 1) m1)
        ((and (number? m1)  (number? m2))  (* m1 m2))
        (else (list '* m1 m2))))

(define (first-plus-sign-pos exp)
  (define (iter s i)
    (cond ((null? s) -1) ; non-exisitent
          ((eq? (car s) '+) i)
          (else (iter (cdr s) (+ i 1)))))
  (iter exp 1))

(define (sum? x)
  (and (pair? x)
       (> (first-plus-sign-pos x) -1)))

(define (addend s)
  (let* ((pos (first-plus-sign-pos s))
         (res (segment-before s pos)))
    (if (null? (cdr res))
      (car res)
      res)))

(define (augend s)
  (let* ((pos (first-plus-sign-pos s))
         (res (segment-after s pos)))
    (if (null? (cdr res))
      (car res)
      res)))

(define (product? x)
  (and (pair? x)
       (= (first-plus-sign-pos x) -1))) 

(define (multiplier p)  (car p))

(define (multiplicand p)
  (if (null? (cdddr p))
      (caddr p)
      (cddr p)))

(define (deriv exp var)
  (cond ((number? exp)
         0)

        ((variable? exp)
         (if (same-variable? exp var) 1 0))

        ((sum? exp)
         (make-sum (deriv (addend exp) var)
                   (deriv (augend exp) var)))

        ((product? exp)
         (make-sum
           (make-product (multiplier exp)
                         (deriv (multiplicand exp) var))
           (make-product (deriv (multiplier exp) var)
                         (multiplicand exp))))

        (else
          (error "unknown expression type -- DERIV" exp))))

(deriv '(x + 3 * (x + y + 2)) 'x)
(deriv '(x * x + x) 'x)
(deriv '(1024 + x * (x + 5)) 'x)
