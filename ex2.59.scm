(define (element-of-set? x set)
  (cond ((null? set) false)
        ((equal? x (car set)) true)
        (else (element-of-set? x (cdr set)))))

(define (union-set set1 set2)
  (cond ((or (null? set1) (null? set2))
             set2)
        ((element-of-set? (car set1) set2)
         (union-set (cdr set1) set2))
        (else
          (union-set (cdr set1) (cons (car set1) set2)))))

(union-set '(a b c) '(b c d))
(union-set '(a b c) '(a))
(union-set '(a b c) '())
(union-set '(a b c) '(a b c))
