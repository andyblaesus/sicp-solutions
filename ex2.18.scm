(define (last-pair l)
  (define (find l)
    (if (null? (cdr l))
      (car l)
      (find (cdr l))))
  (list (find l))
  )

(define (all-but-last l)
  (if (null? (cdr l))
    ()
    (cons (car l) (all-but-last (cdr l)))))

(define (reverse l)
  (if (null? (cdr l))
    (cons (car l) ())
    (cons (car (last-pair l)) (reverse (all-but-last l)))))

(reverse (list 1 4 9 16 25))
