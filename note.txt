Common mistakes

1. 
(1 2 3)
This is not a list.
It applies "1" to 2 and 3.

2.
(cond (pred1 exp1)
      (pred2 exp2)
      (exp3))
Should have "else"!
----

Exercise 1.41, about (double double), really confused me. Should return to it later.

Exercise 2.16, about interval of (R1R2)/(R1+R2), given intervals of R1 and R2, inspring.

Exercise 2.27, deep-reverse of list. Struggled some time. 

Exercise 2.28, can't pass.

Exercise 2.42&2.43, can't solve unassisted.
