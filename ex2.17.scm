(define (last-pair l)
  (define (find l)
    (if (null? (cdr l))
      (car l)
      (find (cdr l))))
  (list (find l))
  )

(last-pair (list 23 72 149 34))
