(define zero (lambda (f) (lambda (x) x)))

(define (add-1 n)
  (lambda (f) (lambda (x) (f ((n f) x)))))

(define one 
  (lambda (f) (lambda (x) (f x))))

(define two
  (lambda (f) (lambda (x) (f (f x)))))

(define (plus a b)  ; if use "+", unchurch will fail
  (lambda (f)
    (lambda (z)
      ((a f) ((b f) z)))))

(define (unchurch n)   ;idea taken from blog JoT's Jottings 
  (define (inc i) (+ i 1))
  ((n inc) 0))

; Testing
(unchurch zero)
(unchurch two)
(unchurch (plus one two))
(unchurch (plus two (plus one two)))
