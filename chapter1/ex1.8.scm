;;;; Cubic root with Newton's method

(define THRESOLD 0.000001 )
(define INITIAL-GUESS 1.0)
(define INITIAL-PREVIOUS 1000000)

(define (cbrt x)
  (cbrt-iter INITIAL-GUESS INITIAL-PREVIOUS x))

(define ( cbrt-iter guess previous-guess x )
  (if ( goodenough? guess previous-guess x )
    guess
    (cbrt-iter (improve guess x)
               guess
               x)))

(define (goodenough? guess previous-guess x)
  (< (abs  (- guess previous-guess)) THRESOLD ))

(define (improve guess x)
  (/ (+ (/ x (* guess guess))
        (* 2 guess))
     3
    )
  )

(cbrt 1.0e50)
