(define double
  (lambda (x) (+ x x)))

(define (halve x) (halve-iter 1 x))
(define (halve-iter half target)
  (if (= (double half) target)
    half
    (halve-iter (+ 1 half) target)))

(define ( * a b )
  (cond ((= b 0)
         0)
        ((= (remainder b 2) 0)
         (double (* a (halve b))))
        (else
          (+ a (* a (- b 1))))))

(* 10 6)
