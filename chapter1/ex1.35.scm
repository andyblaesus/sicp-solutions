; Because (1+sqrt(5)/2) satisfies x = 1 + 1/x.

(define tolerance 0.00001)

(define (fixed-point f guess)

  (define (closeenough? a b)
    (< ( abs (- a b)) tolerance))

  (if (closeenough? guess (f guess))
    guess
    (fixed-point f (f guess))))

(fixed-point (lambda (x) (+ 1 (/ 1 x))) 1.0)
