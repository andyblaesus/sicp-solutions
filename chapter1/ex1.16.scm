(define (expt b n)
  (cond ( (= 0 n) 1 )
        ( (= 0 (remainder n 2)) (expt (* b b) (/ n 2)) )
        ( else (* b (expt b (- n 1))))))
