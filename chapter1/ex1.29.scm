(define (sim-integral f a b n)

  (define h (/ (- b a) n))

  (define (coeff n)
    (if (even? n) 2 4))

  (define (sum-middle-terms f n)
    (if (= n 0) 0
      (+ (* (coeff n)
            (f (+ a (* n h))))
         (sum f (- n 1)))))

  (* (/ h 3.0)
     (+ (f a)
        (f b)
        (sum-middle-terms f (- n 1)))))

(sim-integral
  (lambda (x) (* x x x))
  0
  1
  100
  )
