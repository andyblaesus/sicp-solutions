(define tolerance 0.00001)

(define (fixed-point f guess)

  (define (closeenough? a b)
    (< ( abs (- a b)) tolerance))

  (display "\n guess=")
  (display guess)
  (if (closeenough? guess (f guess))
    guess
    (fixed-point f (f guess))))

(define (average-damp f)
  (lambda (x)
    (/ (+ x (f x))
       2)))

(define (repeated f count)
  (lambda (x)
    (if (= count 0)
      x
      (f ((repeated f (- count 1)) x)))))

(define (average-damp-n f n)
  (lambda (x)
    (((repeated average-damp n) f) x)))

(define (pow x power)
  ((repeated (lambda (i)
               (* x i))
             (- power 1))
   x))

(define (root x n)

  (define (target-f y)
    (/ x
       (pow y
            (- n 1))))

  (let ((damp-count
          (truncate (/ (log n)
                       (log 2)))))
    (fixed-point (average-damp-n target-f damp-count)
                 1.0)))

(root 100000 10)
(root 2 2)
(root 1024 10)
