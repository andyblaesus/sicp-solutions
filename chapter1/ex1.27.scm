(define carmi-nums (list 561 1105 1729 2465 2821 6601))

(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))        

(define (fermat-test-all n)

  (define (fit? a p)     ; Does a and p fits Fermat's Little Theorem?
    (= (expmod a p p)
       (remainder a p)))

  (define (check a p)
    (cond ((> (square a) p) "PASS")
          ((not (fit? a p)) "Composite")
          (else (check (+ a 1) p))))

  (check 2 n))

(define (test-list L)
      (newline)
      (display (car L))
      (display " ")
      (display ( fermat-test-all (car L)))
      (if (not (null? (cdr L)))
        (test-list (cdr L))))

(test-list carmi-nums)

;; For testing validity of implementation
; (define composites (list 4 8 12 99 15000))
; (define primes (list 3 5 7 11 37 47))
; (test-list composites)
; (test-list primes)
