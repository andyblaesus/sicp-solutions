(define tolerance 0.0001)

(define (iterative-improve goodenough? improve)
  (lambda (guess)
    (if (goodenough? guess (improve guess))
    guess
    ((iterative-improve goodenough? improve)
     (improve guess)))))

(define (close? a b)
  (< (abs (- a b)) tolerance))

(define (sqrt x)

  (define (improve guess)
    (/ (+ guess
          (/ x guess))
       2))

  ((iterative-improve close? improve) 1.0))

(sqrt 2)

(define (fixed-point f initial-guess)
  (define (improve guess)
    (f guess))
  ((iterative-improve close? improve) initial-guess))

; Testing

(define (sqrt-new n)
  (fixed-point (lambda (x) (/ (+ x
                                 (/ n x))
                              2))
               1.0))

(sqrt-new 2)
