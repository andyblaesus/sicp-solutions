(define (cont-frac N D k)
  (define (iter i)
    (if (= i k) (/ (N i) (D i))
      (/ (N i) (+ (D i)
                  (iter (+ i 1))))))
  (iter 1))


(define (tan-cf x k)
  (define (D i) (- (* 2 i) 1))
  (define (N i) (* -1 (square x)))

  (/ (cont-frac N D k)
     (* -1.0 x)))

(tan-cf 1 10)
(tan 1)
