; ; Recusive
; (define (accumulate combiner null-value term a next b)
;   (if (> (next a) b)
;     (term a)
;     (combiner (term a)
;               (accumulate combiner null-value term (next a) next b))))

; ; Iterative
(define (accumulate combiner null-value term a next b)
  (define (iter a result)
    (if (> (next a) b)
      (combiner (term a) result)
      (iter (next a) (combiner (term a) result))))
  (iter a null-value))

(define (sum term a next b)
  (accumulate + 0 term a next b))

(define (product term a next b)
  (accumulate * 1 term a next b))


(define (identity x) x)
(define (inc x) (+ x 1))

(product identity 1 inc 10)
