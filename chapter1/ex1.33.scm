; Implementation of (prime? n)

(define (expmod base exp m)
  (define (NT-root? n m)
    (cond ((= n 1) #f)
          ((= n -1) #f)
          ((= (remainder (square n) m) 1) #t)
          (else #f)))
  (cond ((= exp 0) 1)
        ((even? exp)
         (if (NT-root? (/ exp 2) m)
           0
           (remainder (square (expmod base (/ exp 2) m))
                      m)))
        (else
          (remainder (* base (expmod base (- exp 1) m))
                    m))))        

(define (fermat-test n t)
  (define (fit? a)  ;Check if a^(p-1) = 1 (mod n)
    (= (expmod a (- n 1) n)
       1))
  (cond ((= t 0) #t)
        ((not (fit? (+ 1 (random (- n 1))))) #f)
        (else (fermat-test n (- t 1)))))


(define MAXTIMES 100)

(define (prime? n) (fermat-test n MAXTIMES))

; Implementation of filtered-accumulate
(define (filtered-accumulate combiner null-value term a next b predicate)
  (define (iter a result)
    (let ((cond-term (if (predicate a)
                     (combiner (term a) result)
                     result))) 
      (if (> (next a) b)
        cond-term
        (iter (next a) cond-term))))

  (iter a null-value))

; sub-question a
(define (inc n) (+ n 1))

(define (sum-square-prime a b)
  (filtered-accumulate + 0 square a inc b prime?))

(sum-square-prime 2 10)


; sub-question b
(define (identity x) x)

(define (gcd a b)
  (if (= b 0)
    a
    (gcd b (remainder a b))))

(define (rel-prime? n)
  (lambda (i)
    (= 1 (gcd i n))))

(define (sum-coprime n)
  (filtered-accumulate * 1 identity 2 inc (- n 1) (rel-prime? n)))

(sum-coprime 10)
