(A 1 10)
(A (- 1 1) (A 1 (- 10 1)))
(A 0 (A 1 9))
(A 0 (A (- 1 1) (A 1 (- 9 1))))
(A 0 (A 0 (A 1 8)))
(A 0 (A 0 (A 0 (A 1 7))))

(A 0             ;1st
 (A 0            ;2nd
   (A 0          ;3rd
      (A 0       ;4th
       (A 1 6)))))
;;...
(A 0                                        ;1024
   (A 0                                     ;512
      (A 0                                  ;256
         (A 0                               ;128
            (A 0                            ;64
               (A 0                         ;32
                  (A 0                      ;16
                     (A 0                   ;8
                        (A 0                ;4
                           (A 1 1)))))))))) ;2

;Therefore, (A 1 10) == 1024

(A 2 4)
(A 1 (A 2 3))
(A 1 (A 1 (A 2 2)))
(A 1 (A 1 (A 1 (A 2 1))))
(A 1 (A 1 (A 1 2)))
(A 1 (A 1 (A 0 (A 1 1))))
(A 1 (A 1 (A 0 2))) 
(A 1 (A 1 4))
(A 1 (A 0 (A 1 3)))
(A 1 (A 0 (A 0 (A 0 2))))
(A 1 (A 0 (A 0 4)))
(A 1 (A 0 8))
(A 1 16)
;;From what we had before, (A 1 n) == 2^n
65536

(A 3 3)
(A 2 (A 3 2))
(A 2 (A 2 (A 3 1)))
(A 2 (A 2 2))
(A 2 (A 1 (A 2 1)))
(A 2 (A 1 2))
(A 2 (A 0 (A 1 1)))
(A 2 (A 0 2))
(A 2 4)
65536
