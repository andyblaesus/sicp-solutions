(define (smallest-divisor n)
  (sd-iter n 2))

(define (sd-iter n i)
  (cond ((> (square i) n) n)
        ((divides? i n) i)
        (else (sd-iter n (+ i 1)))))

(define (divides? a b)
  (= (remainder b a) 0))

(smallest-divisor 199)   ;199
(smallest-divisor 1999)  ;1999
(smallest-divisor 19999) ;7
