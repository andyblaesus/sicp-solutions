; Yes the run times conform with the prediction.
; 2.7s vs 8.0s


(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (smallest-divisor n)
  (sd-iter n 2))

(define (sd-iter n i)
  (cond ((> (square i) n) n)
        ((divides? i n) i)
        (else (sd-iter n (+ i 1)))))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (search-for-prime start count)
  (timed-prime-test start)
  (cond ((= count 0) "DONE")
        ((prime? start) (search-for-prime (+ start 2) (- count 1)))
        (else (search-for-prime (+ start 2) count))))

(define start1 10000000000000)
(define start2 100000000000000)
(define count 3)

(define start start1)
(search-for-prime (if (even? start) (+ 1 start) start)
                  count) 

(define start start2)
(search-for-prime (if (even? start) (+ 1 start) start)
                  count) 
