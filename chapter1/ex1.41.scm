; The result is 21.
;
; Really tricky. Weiqun Zhang had a very good explanation as follows.
;
; Define a helper procedure quadruple:
;
; (define (quadruple f)
;    (lambda (x) (f (f (f (f x))))))
;
; Then (double double) is (quadruple).
;
; So, ((double (double double)) f)  is
;     ((double quadruple) f)        which is
;     (quadruple (quadruple f))     [!!]
;
; At first, when I got to here, I had something in mind like
; (double (quadruple f)), which lead me to the answer 13.
;
; Names indeed help us understand, don't they?


(define (double f)
  (lambda (x)
    (f (f x))))

(define (inc x)
  (+ x 1))
(((double (double double)) inc) 5)
