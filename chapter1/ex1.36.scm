(define tolerance 0.00001)

(define (fixed-point f guess)

  (define (closeenough? a b)
    (< ( abs (- a b)) tolerance))

  (newline)
  (display guess)
  (if (closeenough? guess (f guess))
    guess
    (fixed-point f (f guess))))

(fixed-point (lambda (x)
               (/ (log 1000)
                  (log x)))
             2.0)
