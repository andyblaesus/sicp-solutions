(define (repeated f count)
  (lambda (x)
    (if (= count 1)
      (f x)
      (f ((repeated f (- count 1)) x)))))

((repeated square 2) 5)
