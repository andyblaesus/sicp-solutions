(define  (func a b c)
  (cond ( ( and (< a c) (< a b) ) (+ b c) )
        ( ( and (< b c) (< b a) ) (+ a c) )
        ( ( and (< c a) (< c b) ) (+ a b) )
        )
  )

(func 5 2 3.1)
