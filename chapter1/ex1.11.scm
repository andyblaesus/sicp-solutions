;;Recursive
(define (f1 n)
  (cond ((< n 3) 3)
        (else (+ (f (- n 1))
                 (* 2 (f (- n 2)))
                 (* 3 (f (- n 3)))))))

;;Iterative
(define (f2 n) (f-iter 2 n 3 3 3))

(define (f-iter counter target pre1 pre2 pre3)
  (cond ((< target 3) 3)
        ((= counter target) pre3)
        (else (f-iter (+ 1 counter)
                      target
                      pre2
                      pre3
                      (combine pre1 pre2 pre3)))
    )
  )

(define (combine pre1 pre2 pre3)
  (+ pre3
     (* 2 pre2)
     (* 3 pre1)))

(f2 5)
