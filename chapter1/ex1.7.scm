(define ( sqrt-iter guess previous-guess x )
  (if ( goodenough? guess previous-guess x )
    guess
    (sqrt-iter (improve guess x)
               guess
               x)))

(define THRESOLD 0.0000000000000001 )
(define INITIAL-GUESS 1.0)
(define INITIAL-PREVIOUS 1000000)

(define (goodenough? guess previous-guess x)
  (< (abs  (- guess previous-guess)) THRESOLD ))

(define (improve guess x)
  (average guess (/ x guess)))

(define average 
  (lambda (a b)
          (/ (+ a b) 2 ))
  )

(define (sqrt x)
  (sqrt-iter INITIAL-GUESS INITIAL-PREVIOUS x)
  )

(sqrt 1024)
