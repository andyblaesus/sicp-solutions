(define (product-recur term a next b)
  (if (> (next a) b)
    (term a)
    (* (term a)
       (product-recur term
                      (next a)
                      next
                      b))))

(define (product-iter term a next b)
  (define (iter a result)
    (if (> (next a) b)
      (* result (term a))
      (iter (next a) (* result (term a)))))
  (iter a 1))

(define (term n)
  (/ (* (* n 2)
        (+ (* n 2)
           2))
     (square (+ (* n 2)
                1))))

(define (next n) (+ n 1))

(define (product a b c d) (product-iter a b c d))

(define (pi k)
  (* 4.0 (product term 1 next k)))

(pi 10000)
