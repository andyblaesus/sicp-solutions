; a. recursive
(define (cont-frac N D k)
  (define (iter i)
    (if (= i k) (/ (N i) (D i))
      (/ (N i) (+ (D i)
                  (iter (+ i 1))))))
  (iter 1))


; b. iterative
(define (cont-frac-iter N D k)

  (define (iter i result)
    (cond ((= i 1)
           result)
          (else
            (iter (- i 1)
                  (/ (N (- i 1))
                     (+ (D (- i 1))
                        result))))))

  (iter k (/ (N k) (D k))))  ;The last round of calculation is inline.

(cont-frac (lambda (i) 1.0)
           (lambda (i) 1.0)
           100)

(cont-frac-iter (lambda (i) 1.0)
                (lambda (i) 1.0)
                1000)

(define (test f n)
  (if (< (abs (- (f n)
              (f (+ n 1))))
         0.0001
         )
    n
    (test f (+ n 1))))

(test (lambda (n) (cont-frac
                    (lambda (i) 1.0)
                    (lambda (i) 1.0)
                    n))
      1)

(test (lambda (n) (cont-frac-iter
                    (lambda (i) 1.0)
                    (lambda (i) 1.0)
                    n))
      1)

; 10 Times
