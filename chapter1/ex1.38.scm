(define (cont-frac N D k)
  (define (iter i)
    (if (= i k) (/ (N i) (D i))
      (/ (N i) (+ (D i)
                  (iter (+ i 1))))))
  (iter 1))


(define (N i) 1.0)
(define (D i)
  (if (= (remainder i 3) 2)
    (* (/ 2 3)
       (+ i 1))
    1))

(define (e k) 
  (+ 2
     (cont-frac N D k)))

(e 100)
