(gcd 206 40)

(if (= 40 0)
  206
  (gcd 40 (remainder 206 40))
  )

;since predicate of if is false...

(gcd 40 (remainder 206 40))

(if (= (remainder 206 40) 0)
  40
  (gcd (remainder 206 40)
       (remainder 40 (remainder 206 40))))

;the first remainder is required for if, therefore computed...
; remainder count = 1

(if (= 6 0)
  40
  (gcd (remainder 206 40)
       (remainder 40 (remainder 206 40))))

;predicate is false

(gcd (remainder 206 40)
     (remainder 40 (remainder 206 40)))

(if (= 0 (remainder 40 (remainder 206 40)))
  (remainder 206 40)
  (gcd (remainder 40
                  (remainder 206 40))
       (remainder (remainder 206 40)
                  (remainder 40 (remainder 206 40)))))

; if requires computing two reminders...
; remainder count = 2

(if (= 0 (remainder 40 6))
  (remainder 206 40)
  (gcd (remainder 40
                  (remainder 206 40))
       (remainder (remainder 206 40)
                  (remainder 40 (remainder 206 40)))) 

; another layer
; remainder count =3

( if (= 0 4)
  (remainder 206 40)
  (gcd (remainder 40
                  (remainder 206 40))
       (remainder (remainder 206 40)
                  (remainder 40 (remainder 206 40))))))

; still false
 
(gcd (remainder 40
                (remainder 206 40) 
     (remainder (remainder 206 40)
                (remainder 40 (remainder 206 40)))))

(if (= (remainder (remainder 206 40)
                  (remainder 40 (remainder 206 40)))
       0)
  (gcd (remainder (remainder 206)
                  (remainder 40 (remainder 206 40))
                  )
       (remainder (remainder 40 (remainder 206 40))
                  (remainder (remainder 206 40)
                             (remainder 40 (remainder 206 40))))))
;...
