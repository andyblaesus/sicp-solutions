(define dx 0.00001)

(define (deriv f x)
  (/ (- (f (+ x dx))
        (f x))
     dx))

(define (newton-method f guess)
  (define (closeenough? guess)
    (< (abs (f guess)) 0.0001)) 
  (if (closeenough? guess)
    guess
    (newton-method f (- guess
                        (/ (f guess)
                           (deriv f guess))))))

(define (cubic a b c)
  (lambda (x) (+ (* x x x)
                 (* a x x)
                 (* b x)
                 c)))

(newton-method (cubic 0 0 -1) 1)
