; For initial count of n, n^2 and n^3, I expect the speed of ratio
; to be 1:2:3
; Actual data: 0.74s vs. 1.72s vs 2.94,
; ratio: 1 : 2.3 : 3.97
; (fermat test 10000 times.)
;
; No. I can't explain the discrepany.


(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (smallest-divisor n)
  (sd-iter n 2))

(define (sd-iter n i)
  (cond ((> (square i) n) n)
        ((divides? i n) i)
        (else (sd-iter n (+ i 1)))))

(define (divides? a b)
  (= (remainder b a) 0))


(define (expmod base exp m)
  (cond ((= exp 0) 1)
        ((even? exp)
         (remainder (square (expmod base (/ exp 2) m))
                    m))
        (else
         (remainder (* base (expmod base (- exp 1) m))
                    m))))        

(define (fermat-test n)
  (define (try-it a)
    (= (expmod a n n) a))
  (try-it (+ 1 (random (- n 1)))))

(define (fast-prime? n times)
  (cond ((= times 0) true)
        ((fermat-test n) (fast-prime? n (- times 1)))
        (else false)))

(define (prime? n) (fast-prime? n TEST-TIMES))

(define (search-for-prime start count)
  (timed-prime-test start)
  (cond ((= count 0) "DONE")
        ((prime? start) (search-for-prime (+ start 2) (- count 1)))
        (else (search-for-prime (+ start 2) count))))

(define amount 100000000000000)
(define start1 amount)
(define start2 (square amount))
(define start3 (* amount amount amount))
(define count 3)
(define TEST-TIMES 10000)  ; for fermet test

(define (test start)
  (search-for-prime (if (even? start) (+ 1 start) start)
                    count)) 
(test start1)
(test start2)
(test start3)
