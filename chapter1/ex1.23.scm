; No. The new method is around 30% faster. Ratio of speed is around 1.4. 
; Data: 2.7s -> 1.9s ; 8s -> 5.9s
;
; Explanation:
; For numbers that divide 2 or 3, which cover 2/3 of all integers,
;   the new methods does not save time.
; For numbers that divide 5, the new method only saves 25% of the steps,
;   rather than the supposed 50%:
;   2 3 4 5 --> 2 3 5
; As the smallest divisor of the test number goes up,
;   the "saving effect" of the number also increases (and approaches 50%).
;   Unfortunately, the amount of such test numbers decreases 
;   thereby limiting the actually time saved.


(define (timed-prime-test n)
  (newline)
  (display n)
  (start-prime-test n (runtime)))

(define (start-prime-test n start-time)
  (if (prime? n)
      (report-prime (- (runtime) start-time))))

(define (report-prime elapsed-time)
  (display " *** ")
  (display elapsed-time))

(define (smallest-divisor n)
  (sd-iter n 2))

(define (sd-iter n i)
  (define (next n)
    (if (= n 2) 3 (+ n 2)))
  (cond ((> (square i) n) n)
        ((divides? i n) i)
        (else (sd-iter n (next i)))))

(define (divides? a b)
  (= (remainder b a) 0))

(define (prime? n)
  (= n (smallest-divisor n)))

(define (search-for-prime start count)
  (timed-prime-test start)
  (cond ((= count 0) "DONE")
        ((prime? start) (search-for-prime (+ start 2) (- count 1)))
        (else (search-for-prime (+ start 2) count))))

(define start1 10000000000000)
(define start2 100000000000000)
(define count 3)

(define start start1)
(search-for-prime (if (even? start) (+ 1 start) start)
                  count) 

(define start start2)
(search-for-prime (if (even? start) (+ 1 start) start)
                  count) 
