(define dx 0.0001)

(define (repeated f count)
  (lambda (x)
    (if (= count 1)
      (f x)
      (f ((repeated f (- count 1)) x)))))

(define (average a b c)
  (/ (+ a b c)
     3))

(define (smooth f)
  (lambda (x)
    (average (f (- x dx))
             (f x)
             (f (+ x dx)))))

(define (smooth-n f n)
  ((repeated smooth n) f))
