(define (expmod base exp m)

  (define (NT-root? n m)
    (cond ((= n 1) #f)
          ((= n -1) #f)
          ((= (remainder (square n) m) 1) #t)
          (else #f)))

  (cond ((= exp 0) 1)
        ((even? exp)
         (if (NT-root? (/ exp 2) m)
           0
           (remainder (square (expmod base (/ exp 2) m))
                      m)))
        (else
          (remainder (* base (expmod base (- exp 1) m))
                    m))))        

(define (fermat-test n t)

  (define (fit? a)  ;Check if a^(p-1) = 1 (mod n)
    (= (expmod a (- n 1) n)
       1))

  (cond ((= t 0) "Prime")
        ((not (fit? (+ 1 (random (- n 1))))) "Composite")
        (else (fermat-test n (- t 1)))))

(define (test-list L)
      (newline)
      (display (car L))
      (display " ")
      (display (fermat-test (car L) MAXTIMES))
      (if (not (null? (cdr L)))
        (test-list (cdr L))))


(define MAXTIMES 100)
(define composites (list 4 8 9 12 99 1024 15000))
(define primes (list 3 5 7 11 37 47 131 1973 7879))
(define carmi-nums (list 561 1105 1729 2465 2821 6601))

(test-list carmi-nums)
(test-list composites)
(test-list primes)


; (test-list
;     (let ((p (open-input-file "primes-to-100k.txt")))
;       (let f ((x (read p)))
;         (if (eof-object? x)
;             (begin
;               (close-input-port p)
;               '())
;             (cons x (f (read p)))))))
