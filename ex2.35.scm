(define (enumerate-tree tree)
  (cond ((null? tree) '())
        ((not (pair? tree)) (list tree))
        (else (append (enumerate-tree (car tree))
                      (enumerate-tree (cdr tree))))))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (count-leaves t)
  (accumulate (lambda (this-node remaining-stuff)
                (+ 1 remaining-stuff))
              0
              (enumerate-tree t)))

; Test 
(newline)
(count-leaves '())
(count-leaves (list 0 0 (list 0)))
(count-leaves (list 0 0 (list 0 0 (list 0) 0)))
