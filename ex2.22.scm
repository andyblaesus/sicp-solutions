;; 1. Because the order in the last (cons) is wrong.
;; 2. Because it creates a "reverse list", where the left node points to
;; the remaining list, and the right node points to the value.
;; A correct implementation (without calling reverse) is as follows.

(define (square-list items)
  (define (iter remaining-items answer)
    (if (null? remaining-items)
      answer
      (iter (cdr remaining-items)
            (append answer (cons (square (car remaining-items))
                                 '())))))
  (iter items '()))


(square-list (list 1 2 3 4))
