(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (tree->list-1 tree)
  (if (null? tree)
      '()
      (append (tree->list-1 (left-branch tree))
              (cons (entry tree)
                    (tree->list-1 (right-branch tree))))))

(define (tree->list-2 tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
        result-list
        (copy-to-list (left-branch tree)
                      (cons (entry tree)
                            (copy-to-list (right-branch tree)
                                          result-list)))))
  (copy-to-list tree '()))

(define tree-A (make-tree 7
                          (make-tree 3
                                     (make-tree 1 '() '())
                                     (make-tree 5 '() '()))
                          (make-tree 9
                                     '()
                                     (make-tree 11 '() '()))))

(define tree-B (make-tree 3
                          (make-tree 1 '() '())
                          (make-tree 7
                                     (make-tree 5 '() '())
                                     (make-tree 9
                                                '()
                                                (make-tree 11 '() '())))))
(define tree-C (make-tree 5
                          (make-tree 3
                                     (make-tree 1 '() '())
                                     '())
                          (make-tree 9
                                     (make-tree 7 '() '())   
                                     (make-tree 11 '() '()))))
; Control group
(equal?  (tree->list-1 tree-A) (make-tree '() '() '()))

; Experimental group
(equal?  (tree->list-1 tree-A) (tree->list-2 tree-A))
(equal?  (tree->list-1 tree-B) (tree->list-2 tree-B))
(equal?  (tree->list-1 tree-C) (tree->list-2 tree-C))

;; a. For the three tress in the textbook, the two procedures produced the same results.
;; b. Procedure 1 uses append, which is slower as it needs to "find the tail" every time. Procedure 2 uses cons.
