(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (element-of-set? x set)
  (cond ((null? set) false)
        ((= x (car set)) true)
        ((< x (car set)) false)
        (else (element-of-set? x (cdr set)))))

(define (union-list set1 set2)
  (let ((x1 (if (null? set1) 0 (car set1)))
        (x2 (if (null? set2) 0 (car set2))))
    (cond ((null? set1) 
           set2)
          ((null? set2)
           set1)
          ((= x1 x2)
           (cons x1
                 (union-list (cdr set1) (cdr set2))))
          ((> x1 x2)
           (cons x2
                 (union-list set1 (cdr set2))))
          ((< x1 x2)
           (cons x1
                 (union-list (cdr set1) set2))))))

(define (intersection-list set1 set2)
  (if (or (null? set1) (null? set2))
      '()    
      (let ((x1 (car set1)) (x2 (car set2)))
        (cond ((= x1 x2)
               (cons x1
                     (intersection-list (cdr set1)
                                        (cdr set2))))
              ((< x1 x2)
               (intersection-list (cdr set1) set2))
              ((< x2 x1)
               (intersection-list set1 (cdr set2)))))))

(define (tree->list tree)
  (define (copy-to-list tree result-list)
    (if (null? tree)
        result-list
        (copy-to-list (left-branch tree)
                      (cons (entry tree)
                            (copy-to-list (right-branch tree)
                                          result-list)))))
(copy-to-list tree '()))

(define (list->tree elements)
  (car (partial-tree elements (length elements))))

(define (partial-tree elts n)
  (if (= n 0)
      (cons '() elts)
      (let ((left-size (quotient (- n 1) 2)))
        (let ((left-result (partial-tree elts left-size)))
          (let ((left-tree (car left-result))
                (non-left-elts (cdr left-result))
                (right-size (- n (+ left-size 1))))
            (let ((this-entry (car non-left-elts))
                  (right-result (partial-tree (cdr non-left-elts)
                                              right-size)))
              (let ((right-tree (car right-result))
                    (remaining-elts (cdr right-result)))
                (cons (make-tree this-entry left-tree right-tree)
                      remaining-elts))))))))

(define (process-set set1 set2 action)
  (let* ((left-list (tree->list set1))
         (right-list (tree->list set2))
         (processed-list (action left-list right-list)))
    (list->tree processed-list)))

(define (union-set set1 set2)
  (process-set set1 set2 union-list))

(define (intersection-set set1 set2)
  (process-set set1 set2 intersection-list))


;test
;
(define set1 (make-tree 2
                        (make-tree 1 '() '())
                        (make-tree 3 '() '())))

(define set2 (make-tree 5
                        (make-tree 3 '() '())
                        (make-tree 4 '() '())))

(union-set set1 set2)
(intersection-set set1 set2)
