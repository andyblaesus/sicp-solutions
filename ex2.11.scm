
(define (make-interval a b)
   (if (>= b a) (cons a b)
     "Error: upper boundary less than lower boundary."))
(define (lower-bound x) (car x))
(define (upper-bound x) (cdr x))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (sub-interval x y)
  (make-interval 
    (- (lower-bound x) (upper-bound y))
    (- (upper-bound x) (lower-bound y))))

(define (mul-interval x y)
  (let ((a (lower-bound x))
        (b (upper-bound x))
        (c (lower-bound y))
        (d (upper-bound y)))  ;short names for the boundaries

        (let ((x++ (and (>= a 0) (>= b 0)))
              (x-+ (and (<  a 0) (>= b 0)))
              (x-- (and (<  a 0) (<  b 0)))
              (y++ (and (>= c 0) (>= d 0)))
              (y-+ (and (<  c 0) (>= d 0)))
              (y-- (and (<  c 0) (<  d 0))))
              ; possible cases for x and y

          (cond
            ((and x++ y++) (make-interval (* a c) (* b d)))
            ((and x-+ y++) (make-interval (* a d) (* b d)))
            ((and x-- y++) (make-interval (* a d) (* b c)))
            ((and x-+ y-+) (make-interval (min (* a d) (* b c))
                                          (max (* a c) (* b d))))
            ((and x-- y-+) (make-interval (* a d) (* a c)))
            ((and x-- y--) (make-interval (* b d) (* a c)))


            ; reduce these cases to defined cases, applying symmetry
            ((or (and x++ y-+) (and x++ y--) (and x-+ y--))
             (mul-interval y x))))))

(define (div-interval x y)
  (if (< (* (upper-bound y) (lower-bound y)) 0) 
    (mul-interval x
                  (make-interval (/ 1.0 (upper-bound y))
                                 (/ 1.0 (lower-bound y))))
  "Divided by a interval spans zero"))

; Test
(define (test a b c d)
  (mul-interval (make-interval a b)
                (make-interval c d)))

(test 1 2 3 4)
(test 0 1 2 3)
(test -1 0 -4 5)
(test -1 0 0 4)
(test -2 -1 -4 -3)
(test -1 2 -4 -3)
(test -1 2 -3 4)
(test -10 2 -3 4)
(test 1 2 -3 4)
