(define (entry tree) (car tree))
(define (left-branch tree) (cadr tree))
(define (right-branch tree) (caddr tree))
(define (make-tree entry left right)
  (list entry left right))

(define (key record)
  (car record))

(define (lookup given-key set-of-records)
  (let ((current-entry (entry set-of-records))
        (left-tree (left-branch set-of-records))
        (right-tree (right-branch set-of-records)))
    (cond ((= given-key (key current-entry))
           current-entry)
          ((> given-key (key current-entry))
           (lookup given-key right-tree))
          ((< given-key (key current-entry))
           (lookup given-key left-tree)))))


; Testing

(define nodeA (cons 4 "I should be 4"))
(define nodeB (cons 2 "This is 2"))
(define nodeC (cons 1 "1."))
(define nodeD (cons 3 "Three!"))
(define nodeE (cons 5 "Five it is."))

(define records
  (make-tree nodeA
             (make-tree nodeB
                        (make-tree nodeC '() '())
                        (make-tree nodeD '() '()))
             (make-tree nodeE '() '())))

(map (lambda (key)
       (display key)
       (display ": ")
       (display (lookup key records))
       (newline))
     (list 1 2 3 4 5))
