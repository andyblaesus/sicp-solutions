(define (deep-reverse l)
  (cond ((null? l) '())
        ((pair? (car l))
         (append (deep-reverse (cdr l))
                 (list (deep-reverse (car l)))))
        (else
          (append (deep-reverse (cdr l))
                  (list (car l))))))

(define A (list 1 2 3 4 5))
(define B (list 1 2 (list 3 4 5)))
(define C (list 1 (list 2 3) (list 4 (list 5))))

; Testing
(newline)
A
(deep-reverse A)

B
(deep-reverse B)

C
(deep-reverse C)
