(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (fold-left op initial sequence)
  (define (iter result rest)
    (if (null? rest)
        result
        (iter (op result (car rest))
              (cdr rest))))
  (iter initial sequence))

(define fold-right accumulate)

(define (reverse-1 sequence)
  (fold-right (lambda (x y)
                (append y (list x)))
              '()
              sequence))

(define (reverse-2 sequence)
  (fold-left (lambda (x y)
               (cons y x))
             '()
             sequence))


; Testing
(define S (list 1 2 3 (list 4 5) 6))
(reverse-1 S)
(reverse-2 S)
