(define (make-segmant v-start v-end)
(cons v-start v-end))

(define (start-segmant s) (car s))
(define (end-segmant s) (cdr s))
