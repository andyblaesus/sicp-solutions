(define (make-interval a b)
   (if (>= b a) (cons a b)
     (error "Upper boundary less than lower bondary")))
(define (lower-bound x) (car x))
(define (upper-bound x) (cdr x))

(define (add-interval x y)
  (make-interval (+ (lower-bound x) (lower-bound y))
                 (+ (upper-bound x) (upper-bound y))))

(define (sub-interval x y)
  (make-interval 
    (- (lower-bound x) (upper-bound y))
    (- (upper-bound x) (lower-bound y))))

(define (mul-interval x y)
  (let ((a (lower-bound x))
        (b (upper-bound x))
        (c (lower-bound y))
        (d (upper-bound y)))  ;short names for the boundaries

        (let ((x++ (and (>= a 0) (>= b 0)))
              (x-+ (and (<  a 0) (>= b 0)))
              (x-- (and (<  a 0) (<  b 0)))
              (y++ (and (>= c 0) (>= d 0)))
              (y-+ (and (<  c 0) (>= d 0)))
              (y-- (and (<  c 0) (<  d 0))))
              ; possible cases for x and y

          (cond
            ((and x++ y++) (make-interval (* a c) (* b d)))
            ((and x-+ y++) (make-interval (* a d) (* b d)))
            ((and x-- y++) (make-interval (* a d) (* b c)))
            ((and x-+ y-+) (make-interval (min (* a d) (* b c))
                                          (max (* a c) (* b d))))
            ((and x-- y-+) (make-interval (* a d) (* a c)))
            ((and x-- y--) (make-interval (* b d) (* a c)))


            ; reduce these cases to defined cases, applying symmetry
            ((or (and x++ y-+) (and x++ y--) (and x-+ y--))
             (mul-interval y x))))))

(define (div-interval x y)
  (if (> (* (upper-bound y) (lower-bound y)) 0) 
    (mul-interval x
                  (make-interval (/ 1.0 (upper-bound y))
                                 (/ 1.0 (lower-bound y))))
  (error "Divided by a interval spans zero")))

(define (make-center-percent center percent)
  (if (> center 0)
    (make-interval (* center (- 1 percent))
                   (* center (+ 1 percent)))
    (make-interval (* center (+ 1 percent))
                   (* center (- 1 percent)))))

(define (center i)
  (/ (+ (lower-bound i) (upper-bound i)) 2))

(define (percent i)
  (abs (/ (- (upper-bound i) (lower-bound i))
          (+ (upper-bound i) (lower-bound i)))))

; Test
(define x (make-interval 1 2))
(define y (make-interval -2 -1))
