(define (make-mobile left right)
  (list left right))

(define (make-branch length structure)
  (list length structure))

; a

(define (left-branch mobile)
  (if (pair? mobile)
    (car mobile)
    '()))

(define (right-branch mobile)
  (if (pair? mobile)
    (car (cdr mobile))
    '()))

(define (branch-length branch)
  (car branch))

(define (branch-structure branch)
  (car (cdr branch)))

; b

(define (total-weight mobile)
  (if (pair? mobile)
    (+ (total-weight (branch-structure (left-branch mobile)))
       (total-weight (branch-structure (right-branch mobile))))
    mobile))

; c
(define (balanced? mobile)
  (if (not (pair? mobile))
    #t
    (let ((left-length (branch-length (left-branch mobile)))
          (right-length (branch-length (right-branch mobile)))
          (left-structure (branch-structure (left-branch mobile)))
          (right-structure (branch-structure (right-branch mobile)))
          (left-weight (total-weight
                         (branch-structure (left-branch mobile))))
          (right-weight (total-weight
                          (branch-structure (right-branch mobile)))))
      (if (not (= (* left-length left-weight)
                  (* right-length right-weight)))
        #f
        (and (balanced? left-structure)
             (balanced? right-structure))))))

; Testing

(define B1 (make-branch 1 100))
(define B2 (make-branch 2 50))
(define B3 (make-branch 2 (make-mobile B1 B2)))
(define B4 (make-branch 300 1))

(define M1 (make-mobile B1 B1))
(define M2 (make-mobile B1 B2))
(define M3 (make-mobile B2 B3))
(define M4 (make-mobile B3 B4))

(newline)
(total-weight M1)
(total-weight M2)
(total-weight M3)

(balanced? M1)
(balanced? M2)
(balanced? M3)
(balanced? M4)
