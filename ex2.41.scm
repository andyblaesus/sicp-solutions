(define (filter predicate sequence)
  (cond ((null? sequence) '())
        ((predicate (car sequence))
         (cons (car sequence)
               (filter predicate (cdr sequence))))
        (else (filter predicate (cdr sequence)))))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (enumerate-interval low high)
  (if (> low high) '()
    (cons low (enumerate-interval (+ low 1) high))))

(define (remove item sequence)
  (filter (lambda (x) (not (= x item)))
          sequence))

(define (flatmap proc seq)
  (accumulate append '() (map proc seq)))

(define (find-triplet n s)
  (filter
    (lambda (triplet)
      (let ((i (car triplet))
            (j (car (cdr triplet)))
            (k (car (cdr (cdr triplet)))))
        (= s (+ i j k))))
    (flatmap
      (lambda (i)
        (flatmap
          (lambda (j)
            (flatmap
              (lambda (k)
                (list (list i j k)))
              (remove j (remove i (enumerate-interval 1 n)))))
          (remove i (enumerate-interval 1 n))))
      (enumerate-interval 1 n))))

(newline)
(find-triplet 5 5)
(find-triplet 5 6)
(find-triplet 5 9)
(find-triplet 5 13)
