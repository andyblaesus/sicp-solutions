(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (accumulate-n op init seqs)
  (if (null? (car seqs))
    '() 
    (cons (accumulate op init (map car seqs))
          (accumulate-n op init (map cdr seqs))))) 

(define (dot-product v w)
  (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
  (map (lambda (row)
         (dot-product row v))
       m))

(define (transpose mat)
  (accumulate-n cons '() mat))

(define (matrix-*-matrix m n)
  (let ((cols (transpose n)))
    (map (lambda (row)
           (matrix-*-vector cols row))
         m)))


; Test

(define m (list (list 1 2)
                (list 3 4)
                (list 5 6)))
(define v (list 7 8))
(define n (list (list 7 8)
                (list 9 0)))

; (dot-product v v)
; (matrix-*-vector m v)
; (transpose m)
(matrix-*-matrix m n)
; (map * (list 1 2) (list 7 9))
