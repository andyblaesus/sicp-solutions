(define (make-point x y) (cons x y))
(define (x-point p) (car p))
(define (y-point p) (cdr p))

(define (print-point p)
  (newline)
  (display "(")
  (display (x-point p))
  (display ", ")
  (display (y-point p))
  (display ")"))

(define (make-rect1 left-bottom x-width y-height)
  (cons left-bottom (cons x-width y-height)))

(define (make-rect2 left-bottom right-upper)
  (cons left-bottom (cons (- (x-point right-upper)
                             (x-point left-bottom))
                          (- (y-point right-upper)
                             (y-point left-bottom)))))

(define (left-bottom rect)
  (car rect))

(define (x-width rect)
  (car (cdr rect)))

(define (y-height rect)
  (cdr (cdr rect)))

(define (area rect)
  (* (x-width rect)
     (y-height rect)))

(define p (make-point 1 1))
(define q (make-point 3 4))
(define rect1 (make-rect1 p 2 3))
(define rect2 (make-rect2 p q))

(area rect1)
(area rect2)

