;; This solution fails on A/A, but give more accurate results than the naive
;; implementionon some other situations, such as
;; (R1*R2)/(R1 + R2) vs (1/(1/R1 + 1/R2)).
;; The procedure (calculate) takes a procedure and exactly two intervals of
;; possible ranges of the variables and calculate the interval of the outcome
;; of the procedure.

;; Foundations
(define (make-interval a b)
   (if (>= b a) (cons a b)
     (error "Upper boundary less than lower bondary")))
(define (lower-bound x) (car x))
(define (upper-bound x) (cdr x))

;; Reduce the space of x * y into N^2 points, calculate interval at each point,
;; and use the maximum as upper boundary, and minimum as lower boundary.

(define (calculate f x-interval y-interval)

  (define N 100)  ; divide x and y into N parts

  ; i is the serial of points, [0..N^2 -1]
  
  (define (x-value i) ;guess of x at ith point
    (+ (lower-bound x-interval)
       (* (- (upper-bound x-interval) (lower-bound x-interval))
          (/ (remainder i N)
             (- N 1)))))

  (define (y-value i) ;guess of y at ith point
    (define (int-div a b)
      (if (= b 0)
        0
        (/ (- a (remainder a b))
           b)))
    (+ (lower-bound y-interval)
       (* (- (upper-bound y-interval) (lower-bound y-interval))
          (/ (int-div i N)
             (- N 1)))))

  (define (search i previous-best select)
    (let ((f-value (f (x-value i) (y-value i))))
      (let ((current-best (select previous-best f-value)))
        (if (= i (- (* N N) 1))
               current-best
               (search (+ i 1) current-best select)))))

  (let ((i-start-point 0)
        (initial-guess (f (x-value 0) (y-value 0))))
    (make-interval
      (search i-start-point initial-guess min)
      (search i-start-point initial-guess max))))

;; Alternative implementation: list processing

(define (calculate-2 f x-interval y-interval)
  (define N 100)  ; divide x and y into N parts

  (define (x-value i) ;guess of x at i=i 
    (+ (lower-bound x-interval)
       (* (- (upper-bound x-interval) (lower-bound x-interval))
          (/ (remainder i N)
             (- N 1)))))

  (define (y-value i) ;guess of y at i=i
    (define (int-div a b)
      (if (= b 0)
        0
        (/ (- a (remainder a b))
           b)))
    (+ (lower-bound y-interval)
       (* (- (upper-bound y-interval) (lower-bound y-interval))
          (/ (int-div i N)
             (- N 1)))))

  (define (from-i-to-N-square-minus-one i) 
    (if (= i (- (* N N) 1))
      (cons i ())
      (cons i (from-i-to-N-square-minus-one (+ i 1)))))

  (define list-of-xy-combination
    (map (lambda (i) (cons (x-value i) (y-value i)))
         (from-i-to-N-square-minus-one 0)))

  (define (f-pair-version pair)
    (f (car pair) (cdr pair)))

  (define (list-peak l select)
    (define (iter previous-best l-unchecked)
      (if (null? l-unchecked)
        previous-best
        (iter (select previous-best (car l-unchecked))
              (cdr l-unchecked))))
    (iter (car l) (cdr l)))

 (let ((list-of-values (map f-pair-version list-of-xy-combination)))
   (make-interval (list-peak list-of-values min) (list-peak list-of-values max))))

; Test
(define x-interval (make-interval 1 2))
(define y-interval (make-interval 8 9))
(define identity-interval (make-interval 2 4))

(calculate (lambda (x y) (/ (* x y) (+ x y)))
           x-interval
           y-interval)

(calculate (lambda (x y) (/ 1
                            (+ (/ 1 x)
                               (/ 1 y))))
           x-interval
           y-interval)  ;same result!

(calculate (lambda (x y) (/ x y))
             identity-interval
             identity-interval) ;fails!

(calculate-2 (lambda (x y) (/ (* x y) (+ x y)))
             x-interval
             y-interval)

(calculate-2 (lambda (x y) (/ 1
                            (+ (/ 1 x)
                               (/ 1 y))))
             x-interval
             y-interval) ;same result!

(calculate-2 (lambda (x y) (/ x y))
               identity-interval
               identity-interval) ;fail!
