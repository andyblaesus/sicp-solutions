(define (make-leaf symbol weight)
  (list 'leaf symbol weight))

(define (leaf? object)
  (eq? (car object) 'leaf))

(define (symbol-leaf x) (cadr x))

(define (weight-leaf x) (caddr x))

(define (make-code-tree left right)
  (list left
        right
        (append (get-symbol left) (get-symbol right))
        (+ (weight left) (weight right))))

(define (left-branch tree) (car tree))

(define (right-branch tree) (cadr tree))

(define (get-symbol tree)
  (if (leaf? tree)
      (list (symbol-leaf tree))
      (caddr tree)))

(define (weight tree)
  (if (leaf? tree)
      (weight-leaf tree)
      (cadddr tree)))


(define (decode bits tree)
  (define (decode-1 bits current-branch)
    (if (null? bits)
        '()
        (let ((next-branch
               (choose-branch (car bits) current-branch)))
          (if (leaf? next-branch)
              (cons (symbol-leaf next-branch)
                    (decode-1 (cdr bits) tree))
              (decode-1 (cdr bits) next-branch)))))
  (decode-1 bits tree))

(define (choose-branch bit branch)
  (cond ((= bit 0) (left-branch branch))
        ((= bit 1) (right-branch branch))
        (else (error bad bit -- CHOOSE-BRANCH bit))))

(define sample-tree
  (make-code-tree (make-leaf 'A 4)
                  (make-code-tree
                   (make-leaf 'B 2)
                   (make-code-tree (make-leaf 'D 1)
                                   (make-leaf 'C 1)))))
(define (encode message tree)
  (if (null? message)
      '()
      (append (encode-symbol (car message) tree)
              (encode (cdr message) tree))))

(define (encode-symbol char tree)

  (define (element-of? char set)
    (cond ((null? set) #f)
          ((equal? char (car set))
           #t)
          (else (element-of? char (cdr set)))))
  
  (define (search char tree previous-bits)
    (cond ((leaf? tree) previous-bits)
          ((element-of? char
                        (get-symbol (left-branch tree)))
           (search char (left-branch tree) (append previous-bits
                                                   (list 0))))
          ((element-of? char
                        (get-symbol (right-branch tree)))
           (search char (right-branch tree) (append previous-bits
                                                    (list 1))))
          (else (error encode-symbol - Unexpected input tree))))

  (search char tree '()))

(define (adjoin-set x set)
  (cond ((null? set) (list x))
        ((< (weight x) (weight (car set))) (cons x set))
        (else (cons (car set)
                    (adjoin-set x (cdr set))))))

(define (make-leaf-set pairs)
  (if (null? pairs)
      '()
      (let ((pair (car pairs)))
        (adjoin-set (make-leaf (car pair)    ; symbol
                               (cadr pair))  ; frequency
                    (make-leaf-set (cdr pairs))))))

(define (generate-huffman-tree pairs)
  (successive-merge (make-leaf-set pairs)))

(define (successive-merge current-tree)

  (define (insert-node node rest-of-set)
    (cond ((null? rest-of-set)
           (list node))
          ((> (weight node) (weight (car rest-of-set)))
           (cons (car rest-of-set)
                 (insert-node node (cdr rest-of-set))))
          (else (cons node rest-of-set))))

  (if (null? (cdr current-tree))
    (car current-tree)
    (successive-merge (insert-node (make-code-tree (car current-tree)
                                                   (cadr current-tree))
                                   (cddr current-tree)))))

; =    =    =  Testing    =    =   =

(define frequency-table
  (list
    '(A 2) 
    '(NA 16)
    '(BOOM 1) 
    '(SHA 3)
    '(GET 2) 
    '(YIP 9)
    '(JOB 2) 
    '(WAH 1)))

(define code-tree (generate-huffman-tree frequency-table))

(define plaintexts
  (list
    '(GET A JOB)
    '(SHA NA NA NA NA NA NA NA NA)
    '(GET A JOB)
    '(SHA NA NA NA NA NA NA NA NA)
    '(WAH YIP YIP YIP YIP YIP YIP YIP YIP YIP)
    '(SHA BOOM)
    )
  )

(for-each
  (lambda (plaintext)
    (newline)
    (display (encode plaintext code-tree))
    (newline)
    (display "Bit: ")
    (display (length (encode plaintext code-tree)))
    (newline))
  plaintexts)

; 84bit in total.
; If we use fixed-length encoding for 8 symbols, we need 3 bits for each character (because 2^3 = 8). There are 36 symbols in the plaintext, which translate to 108 bits (3 * 36) after encoding.
