(define (filter predicate sequence)
  (cond ((null? sequence) '())
        ((predicate (car sequence))
         (cons (car sequence)
               (filter predicate (cdr sequence))))
        (else (filter predicate (cdr sequence)))))

(define (accumulate op initial sequence)
  (if (null? sequence)
      initial
      (op (car sequence)
          (accumulate op initial (cdr sequence)))))

(define (enumerate-interval low high)
  (if (> low high) '()
    (cons low (enumerate-interval (+ low 1) high))))

(define (unique-pairs n)
  (accumulate append
              '()
              (map
                (lambda (i)
                  (map (lambda (j)
                         (list i j))
                       (enumerate-interval 1 i)))
                (enumerate-interval 1 n))))

(define (prime? n)
  (define (check i)
    (cond ((> (* i i) n)
           #t)
          ((= (remainder n i) 0)
           #f)
          (else
            (check (+ i 1)))))  ; I know...this sucks...
  (check 2))

(define (prime-sum-pairs n)
  (filter
    (lambda (pair)
      (prime? (+ (car pair)
                 (car (cdr pair)))))
    (unique-pairs n)))


; test
(newline)
(unique-pairs 5)
(prime-sum-pairs 5)
