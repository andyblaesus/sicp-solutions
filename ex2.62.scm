(define (element-of-set? x set)
  (cond ((null? set) false)
        ((= x (car set)) true)
        ((< x (car set)) false)
        (else (element-of-set? x (cdr set)))))

(define (union-set set1 set2)
  (let ((x1 (if (null? set1) 0 (car set1)))
        (x2 (if (null? set2) 0 (car set2))))
    (cond ((null? set1) 
           set2)
          ((null? set2)
           set1)
          ((= x1 x2)
           (cons x1
                 (union-set (cdr set1) (cdr set2))))
          ((> x1 x2)
           (cons x2
                 (union-set set1 (cdr set2))))
          ((< x1 x2)
           (cons x1
                 (union-set (cdr set1) set2))))))

(union-set (list 1 2 3) (list 3 4 5))
(union-set (list 5 6 8) (list 3 4 5))
(union-set (list 2 4 6) (list 8 9 10))
(union-set (list 1 3 5) (list 2 4 6))
