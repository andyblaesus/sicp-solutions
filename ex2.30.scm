(define (square x) (* x x))

; Direct definition
;
(define (square-tree-d tree)
  (cond ((null? tree) '())
        ((pair? tree)
         (cons (square-tree-d (car tree))
               (square-tree-d (cdr tree))))
        (else (square tree))))

; Definition using map

(define (square-tree-m tree)
  (map (lambda (sub-tree)
         (if (pair? sub-tree)
           (square-tree-m sub-tree)
           (square sub-tree)))
       tree))

; Testing
(newline)
(define L (list 1 (list 2 (list 3 4) 5) (list 6 7)))
(square-tree-d L)
(square-tree-m L)



