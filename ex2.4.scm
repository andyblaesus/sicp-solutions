(define (cons x y)
  (lambda (selector) (selector x y)))   ; Surprise! function as parameter!

(define (car z)
  (z (lambda (p q) p)))

; 1. Prrof that this definition works.
;
;((car (cons x y)))
;
;((cons x y) (lambda (p q) p))
;
;((lambda (selector) (selector x y))
; (lambda (p q) p)))
;
;((lambda (p q) p) (x y))
;
;x

; 2. definition of cdr

(define (cdr z)
  (z (lambda (p q) q)))

; Test
(define x (cons 20 15))
(car x)
(cdr x)
