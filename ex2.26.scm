;; (list) adds 1 "nil-termination".
;; (append) drops 1 "nil-termination".
;; (cons) neither adds nor drops "nil-termination".
;; Every "nil-termination" creates a list.

(define x (list 1 2 3))
(define y (list 4 5 6))

(append x y) ; (1 2 3 4 5 6)
             ; the nil-termination of x is dropped.

(cons x y)   ; ((1 2 3) 4 5 6)
             ; the nil-termination of y is used to create the list
             ; that contails all elements.
             
(list x y)   ; ((1 2 3) (1 2 3))
             ; Three nil-terminations in total, therefore three lists:
             ; x and y had their own nil-terminations,
             ; (list x y) added a nil-termination, creating a list that
             ; contains x and y.
