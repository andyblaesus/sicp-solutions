(define outline
  (segments->painter
    (list (make-segment (make-vert 0.0 0.0)
                        (make-vert 1.0 0.0))
          (make-segment (make-vert 0.0 0.0)
                        (make-vert 0.0 1.0))
          (make-segment (make-vert 1.0 1.0)
                        (make-vert 1.0 0.0))
          (make-segment (make-vert 1.0 1.0)
                        (make-vert 0.0 1.0)))))

(define X
  (segments->painter
     (list (make-segment (make-vert 0.0 0.0)
                         (make-vert 1.0 1.0))
           (make-segment (make-vert 1.0 0.0)
                         (make-vert 0.0 1.0)))))

(define diamond
  (segments->painter
    (list (make-segment (make-vert 0.0 0.5)
                        (make-vert 0.5 0.0))
          (make-segment (make-vert 0.5 0.0)
                        (make-vert 1.0 0.5))
          (make-segment (make-vert 1.0 0.5)
                        (make-vert 0.5 0.0))
          (make-segment (make-vert 0.5 0.0)
                        (make-vert 0.0 0.5)))))
