(define (make-rat n d)
  (define (gcd a b)
    (if (= b 0)
      a
      (gcd b (remainder a b)))) 
  (define (neg x) (- 0 x))

  (let ((g (gcd n d)))
    (if (> (/ d g) 0)
      (cons (/ n g) (/ d g))
      (cons (neg (/ n g)) (neg (/ d g))))))

(make-rat -2 -2)
(make-rat -4 2)
(make-rat 4 -2)
(make-rat 3 9)
(make-rat -3 -9)
(make-rat -3 -9)
(make-rat 11 -17)
